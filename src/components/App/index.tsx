import React from 'react';
import './App.css';
import MainApp from '../MainApp';
import {useSelector} from "react-redux";


/**
 * Здесь лучше useSelector вынести от сюда и прокинуть пропс
 *
 * const mstp = (state: IState) => {
 *   return {
 *     todos: state.list.todos
 *   }
 * }
 *
 * const connected = connect(mstp, null);
 * type propsApp = ConnectedProps<typeof connected>
 *
 * function App(props: propsApp) {
 *   const {
 *     todos
 *   } = props
 *
 */


function App() {
  const todos = useSelector((state: { list: { todos: any[] } }) => state.list.todos);
  return (
    // туду лист для юзеров:
    <div className="App main">
      <header className="App-header">
        {/*<img src={logo} className="App-logo" alt="logo" />*/}
      </header>
      {/* MAIN APP: */}
      <MainApp todos={todos}/>
      <footer className='App-footer'>
        <a href="https://example.org"
          target="_blank"
          className={"App-footer-link"}>
          All right reserved
        </a>
      </footer>
    </div>
  );
}

export default App;
