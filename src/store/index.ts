import {configureStore} from '@reduxjs/toolkit'


/**
 * Здесь желательно вынести в отдельные модули
 * Сделать отдельную папку modules и туда положить то что косается todo
 * ...createStore(
 *    reducers,
 * )
 * Где reducers будет combineReducers({ list })
 */


export default configureStore({
  reducer: {
    list: (state = {todos: []}, action) => {
      switch (action.type) {
        case 'ADD_TODO': return {


          /**
           *
           * Здесь лучше не производить вычислений, а сделать это в функции
           * А сюда передать готовый результат вычислений
           * {
           *   ...state,
           *   todos: action.payload
           * }
           */

          // const newState = state;
          // newState.todos.push(action.payload);
          // return newState;
        }
        case 'REMOVE_TODO': {
          return {
            ...state,
            todos: state.todos.filter((t: any, index: number) => index !== action.payload),
          }
        }
        case 'CHANGE_TODOS':
          return {
            ...state,
            todos: action.payload,
          }
        default:
          return state;
      }
    }
  }
})
